const MATIC_CONTRACTS = {
    Betting: '0xABF27f74542870402EEb72f9ED85C66A07A1918b',
    BetToken: '0x176783CEF5773bb731f2EBe844a959af26EDcA99',
    Car: '0xe260404C554D76F2BBcd4029C9e74B781099dbc8',
    Driver: '0x4367885f8f917552F571894F550136f6e2326d5E'
}

// previous funder escrow address 0x74A7e123e77Fc23D765585177371a45441A31fCE on Goerli


const GANACHE_CONTRACTS = {
    Betting: '0x652d157027ACe71FF7Ed0b12552eBD9cc969e66D',
    BetToken: '0x11E72F1c45C06D38561a0E36ECaD70Dd75c49a14',
    Car: '0x382e9655972cda3D7193aEBfD6d82b339B1910bc',
    Driver: '0x097e2f974422A4bD90944202c85B30EEe91f6Afb'
}

function getAddress(contractName) {
    if(process.env.TEST_NETWORK === 'matic') {
        return MATIC_CONTRACTS[contractName]
    } else if(process.env.TEST_NETWORK === 'ganache') {
        return GANACHE_CONTRACTS[contractName]
    }
}

module.exports = {
    getAddress
}