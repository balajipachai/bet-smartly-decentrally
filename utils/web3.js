const config = require('../truffle-config.js')
let ENVIRONMENT = 'ganache'; //default environment
if (process.env.TEST_NETWORK !== undefined) {
  ENVIRONMENT = config.networks[process.env.TEST_NETWORK].name;
}

const Web3 = require('web3');

let web3

if (ENVIRONMENT == 'ganache') {
    web3 = new Web3(
        new Web3.providers.HttpProvider(
            config.networks[ENVIRONMENT].protocol +
            '://' +
            config.networks[ENVIRONMENT].host +
            ':' +
            config.networks[ENVIRONMENT].port
        )
    );    
} else {
    web3 = new Web3(new Web3.providers.HttpProvider('https://testnet2.matic.network'))
}

function getAccounts() {
  return new Promise((resolve, reject) => {
    web3.eth.getAccounts(function(error, ethAccounts) {
      if (error) {
        reject({
          status: 'failure',
          message: error.message,
          data: []
        });
      } else {
        // owner = ethAccounts[0]
        resolve({
          status: 'success',
          message: 'Fetched ethereum accounts are: ',
          data: ethAccounts
        });
      }
    });
  });
}

//Gets the contract instance
const getContractInstance = function(contract, flag, abi) {
  // console.log('contract.address in getContractInstance: ', contract.address)
  if (flag) {
    return web3.eth.contract(JSON.parse(abi));
  }
  return web3.eth.contract(contract.abi).at(contract.address);
};

const getNetworkId = function(environment) {
  return config.networks[environment].network_id;
};

const getWeb3Instance = function () {
    return web3
}

module.exports = {
    getAccounts,
    getContractInstance,
    getNetworkId,
    getWeb3Instance
}
