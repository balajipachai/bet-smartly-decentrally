const web3Apis = require('../utils/web3')
const isMined = require('../utils/miningComplete')
const utils = require('../utils/deployedAddresses')

const BettingJSON = require('../build/contracts/Betting.json')

async function setWinner(params) {
    const web3 = web3Apis.getWeb3Instance()
    const accounts = (await web3Apis.getAccounts()).data

    let betTokenInstance = web3.eth.contract(BettingJSON.abi).at(utils.getAddress('Betting'))
    await isMined.checkMining(betTokenInstance.finishRace(params.id, {from: accounts[0], gas: 5500000}))
    console.log({
        status: 'success',
        msg: 'Winner set successfully'
    })
}

setWinner({id: 1})
module.exports = {
    setWinner
}