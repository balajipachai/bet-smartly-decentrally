const Betting = artifacts.require('Betting');
const Car = artifacts.require('Car');

module.exports = function(deployer, network, accounts) {
  const coinbase = accounts[0];
  const TOKEN_NAME = 'CARTOKEN'
  const SYMBOL = 'CART'
  const PROFIT_PERCENT = 20000 // 20%

  deployer.deploy(Car, TOKEN_NAME, SYMBOL, { from: coinbase, gas: 8000000 }).then(function (carInstance) {
    return deployer.deploy(Betting, carInstance.address, PROFIT_PERCENT, { from: coinbase, gas: 8000000 });
  })
};
