pragma solidity ^0.5.0;

import "../openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "../openzeppelin-solidity/contracts/math/SafeMath.sol";
import "../openzeppelin-solidity/contracts/utils/Address.sol";
import "./Car.sol";

contract Betting is Ownable {

    using SafeMath for uint;
    using Address for address;

    uint public constant FRACTIONAL_RATE_SCALING_FACTOR = 10 ** 4;
    uint public constant DIVISIONAL_FACTOR = 10 ** 5;

    uint public profitPercent;
    uint public winnerSharePercent = 10000; // 1 % out of the total commission, to ensure finishRace is executed
    uint public winnerID;

    uint public numberOfWithdrawals = 0;

    Car public carContract;

    mapping (address => mapping(uint => uint)) public betAmount;
    mapping(address => uint) public bettedFor;
    mapping(uint => uint) public countOfBettorFor;
    mapping(address => bool) public isWinnerAmountTransferred;

    /// @param _carContractAddress Address of the Car contract
    /// @param _profitPercent Profit percent that will be given to the successful bettor
    constructor (address _carContractAddress, uint _profitPercent) public {
        require(
            _carContractAddress.isContract(), 
            "in Betting:constructor(). Car address is EOA."
        );
        require(_profitPercent > 0, "in Betting:constructor(). Profit percent is zero.");
        carContract = Car(_carContractAddress);
        profitPercent = _profitPercent;
    }

    /// Modifier onlyCarOwner
    /// @dev Checks the caller to be owner of the car
    modifier onlyCarOwner(uint _winnerId) {
        require(
            msg.sender == carContract.ownerOf(_winnerId),
            "in Betting:onlyCarOwner(). Caller is not the car owner."
        );
        _;
    }

    /// Modifier isWithdrawComplete
    /// @dev Checks whether withdrawal is completed
    modifier isWithdrawComplete() {
        require(
            numberOfWithdrawals > 0,
            "in Betting:isWithdrawComplete(). All withdrawals already executed."
        );
        _;
    }

    /// Modifier afterWithdrawalsComplete
    /// @dev Checks whether functin X has been invoked after withdrwal completion
    modifier afterWithdrawalsComplete() {
        require(
            numberOfWithdrawals == 0,
            "in Betting:afterWithdrawalsComplete(). Cannot withdraw."
        );
        _;
    }

    /// @param _id Id of the car on which you want to bet
    function bet(uint _id) public payable {
        require(msg.sender != address(0), "in Betting:bet(). Caller cannot be address zero.");
        require(msg.value > 0, "in Betting:bet(). Value cannot be zero.");
        require(
            carContract.doesCarExists(_id), 
            "in Betting:bet(). Cannot bet on non-existing car."
        );
        countOfBettorFor[_id] = countOfBettorFor[_id].add(1);
        betAmount[msg.sender][_id] = msg.value;
        bettedFor[msg.sender] = _id;
    }

    /// @param _winnerId Id of the car that won the race
    /// @dev This function can only be executed by the car owner
    function finishRace(uint _winnerId) public onlyCarOwner(_winnerId) {
        winnerID = _winnerId;
        numberOfWithdrawals = countOfBettorFor[_winnerId];
    }

    /// @dev To get the deposited bets plus profit one must execute this function.
    function withdraw() public isWithdrawComplete {
        if (bettedFor[msg.sender] == winnerID && betAmount[msg.sender][winnerID] > 0) {
            uint amount = (
                (profitPercent.mul(betAmount[msg.sender][winnerID])).div(DIVISIONAL_FACTOR)
            );
            betAmount[msg.sender][winnerID] = 0;
            numberOfWithdrawals = numberOfWithdrawals.sub(1);
            msg.sender.transfer(amount);
        } else {
            revert();
        }
    }

    /// @dev This function should be executed by the winner car owner to get their winning share
    function transferWinnerShare(uint _winnerId) public onlyCarOwner(_winnerId) afterWithdrawalsComplete {
        require(
            !isWinnerAmountTransferred[msg.sender], 
            "in Betting:transferWinnerShare(). Already transferred winner share."
        );
        isWinnerAmountTransferred[msg.sender] = true;
        uint amount = (
                (winnerSharePercent.mul(address(this).balance)).div(DIVISIONAL_FACTOR)
        );
        msg.sender.transfer(amount);
    }
}