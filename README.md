# README

### What is this repository for?

- Quick summary

This repository contains the Betting.sol smart contract developed using Solidity for the Game Oasis Hackathon. After the hackathon, I have made some changes to the contract in order to make it robust and third party independent. Test cases may fail, watch this repo to get updated when all the test cases pass.

- Version

  1.0.0

### How do I get set up?

- Summary of set up

```
git clone https://balajipachai@bitbucket.org/balajipachai/bet-smartly-decentrally.git
cd bet-smartly-decentrally
git checkout development
npm install
```

- How to run tests

  `npm run test`

### Contribution guidelines

- Writing tests

      If you are willing to contribute you are always welcome, go through the contract and write test cases for it.
      Create a branch and make a PR.
      I will review the PR and get back to you.

- Other guidelines

  Please follow open source contributing guidelines.

### Who do I talk to?

- Repo owner or admin

  Connect to us on our telegram channel

  https://t.me/bet_smartly_decentrally
